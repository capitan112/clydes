//
//  CladesTests.swift
//  CladesTests
//
//  Created by Oleksiy Chebotarov on 20/09/2019.
//  Copyright © 2019 Oleksiy Chebotarov. All rights reserved.
//

@testable import Clades
import XCTest

class CladesTests: XCTestCase {
    var sut: ViewController!
    var window: UIWindow!

    override func setUp() {
        window = UIWindow()
        setupViewController()
    }

    override func tearDown() {
        window = nil
        super.tearDown()
    }

    // MARK: Test setup

    func setupViewController() {
        let bundle = Bundle.main
        let storyboard = UIStoryboard(name: "Main", bundle: bundle)
        sut = storyboard.instantiateViewController(withIdentifier: "ViewController") as? ViewController
    }

    func loadView() {
        window.addSubview(sut.view)
        RunLoop.current.run(until: Date())
    }

    // MARK: Test doubles

    class PresenterSpy: PresenterProtocol, DataStore {
        var dataFetcherService: DataFetcherProtocol?
        var viewController: DisplayLogic?
        var persons: [Person]?
        var rooms: [Room]?

        var isFetchedPeopleInfo = false
        var isFetchedRoomInfo = false

        func fetchPeopleInfo() {
            isFetchedPeopleInfo = true
        }

        func fetchRoomInfo() {
            isFetchedRoomInfo = true
        }
    }

    // MARK: Tests

    func testShouldFetchPeopleInfoWhenItLoaded() {
        // Given
        let spy = PresenterSpy()
        sut.presenter = spy

        // When
        loadView()

        // Then
        XCTAssertTrue(spy.isFetchedPeopleInfo, "viewDidLoad() should ask the presenter to fetchPeopleInfo")
    }

    func testShouldFetchRoomInfoWhenRoomButtonPressed() {
        // Given
        let spy = PresenterSpy()
        sut.presenter = spy
        let items = ["Persons", "Rooms"]
        let segmentedControl = UISegmentedControl(items: items)
        segmentedControl.selectedSegmentIndex = 1

        // When
        sut.indexChanged(segmentedControl)

        // Then
        XCTAssertTrue(spy.isFetchedRoomInfo, "Selected room button should ask the presenter to fetchRoomInfo")
    }

    func testShouldFetchPersonsInfoWhenPersonsButtonPressed() {
        // Given
        let spy = PresenterSpy()
        sut.presenter = spy
        let items = ["Persons", "Rooms"]
        let segmentedControl = UISegmentedControl(items: items)
        segmentedControl.selectedSegmentIndex = 0

        // When
        sut.indexChanged(segmentedControl)

        // Then
        XCTAssertTrue(spy.isFetchedPeopleInfo, "Selected persons button should ask the presenter to fetchPeopleInfo")
    }
}
