//
//  ViewControlerTest.swift
//  CladesTests
//
//  Created by Oleksiy Chebotarov on 21/09/2019.
//  Copyright © 2019 Oleksiy Chebotarov. All rights reserved.
//

@testable import Clades
import XCTest

class ViewControlerTest: XCTestCase {
    var sut: ViewControllerSpy!

    override func setUp() {
        sut = ViewControllerSpy()
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }

    // MARK: Test doubles

    class ViewControllerSpy: ViewController {
        var isReloadedTableView = false

        override func reloadTableView() {
            isReloadedTableView = true
        }
    }

    func testShouldReloadTableViewWhenItCalledFromPresenter() {
        // Given
        sut.presenter?.viewController? = sut

        // When
        sut.presenter?.viewController?.reloadTableView()

        // Then
        XCTAssertTrue(sut.isReloadedTableView, "Presenter shoudl reload table view, when called it")
    }
}
