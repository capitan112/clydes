//
//  ViewController.swift
//  Clades
//
//  Created by Oleksiy Chebotarov on 20/09/2019.
//  Copyright © 2019 Oleksiy Chebotarov. All rights reserved.
//

import SDWebImage
import UIKit

protocol DisplayLogic: class {
    func reloadTableView()
}

enum Segments: Int, CaseIterable {
    case People
    case Rooms
}

class ViewController: UIViewController, DisplayLogic, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet var segmentControl: UISegmentedControl!
    @IBOutlet var tableView: UITableView!
    private var personsCellHeight: CGFloat = 150
    private var roomsCellHeight: CGFloat = 80

    var presenter: (PresenterProtocol & DataStore)?
    let personsCellID = "PersonsCellID"
    let roomCellID = "RoomCellID"

    // MARK: Object lifecycle

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    // MARK: Setup

    private func setup() {
        let viewController = self
        presenter = Presenter()
        presenter?.viewController = viewController
        presenter?.dataFetcherService = DataFetcherService()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        config()
    }

    private func config() {
        showIndicator()
        presenter?.fetchPeopleInfo()
        tableView.tableFooterView = UIView()
    }

    // MARK: - Protocol methods

    func reloadTableView() {
        performUIUpdatesOnMain { [unowned self] in
            self.tableView.reloadData()
            self.hideIndicator()
        }
    }

    // MARK: - Show/hide indicator

    func showIndicator() {
        performUIUpdatesOnMain {
            LoadingIndicatorView.show()
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
    }

    func hideIndicator() {
        performUIUpdatesOnMain {
            LoadingIndicatorView.hide()
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
    }

    // MARK: - Table view data source

    public func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        let selectedSegment = Segments(rawValue: segmentControl.selectedSegmentIndex)!
        switch selectedSegment {
        case .People:
            return presenter?.persons?.count ?? 0
        case .Rooms:
            return presenter?.rooms?.count ?? 0
        }
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let selectedSegment = Segments(rawValue: segmentControl.selectedSegmentIndex)!

        switch selectedSegment {
        case .People:

            if let person = presenter?.persons?[indexPath.row] {
                let cell = tableView.dequeueReusableCell(withIdentifier: personsCellID, for: indexPath) as! PersonTableViewCell
                cell.configCell(with: person)
                cell.configureAccessibility(person)
                return cell
            }

        case .Rooms:
            if let room = presenter?.rooms?[indexPath.row] {
                let cell = tableView.dequeueReusableCell(withIdentifier: roomCellID, for: indexPath) as! RoomsTableViewCell
                cell.configCell(with: room)
                cell.configureAccessibility(room)

                return cell
            }
        }

        return UITableViewCell()
    }

    func tableView(_: UITableView, heightForRowAt _: IndexPath) -> CGFloat {
        let selectedSegment = Segments(rawValue: segmentControl.selectedSegmentIndex)!

        switch selectedSegment {
        case .People:
            return personsCellHeight
        case .Rooms:
            return roomsCellHeight
        }
    }

    // MARK: - segment selected

    @IBAction func indexChanged(_ sender: UISegmentedControl) {
        showIndicator()
        let selectedSegment = Segments(rawValue: sender.selectedSegmentIndex)!
        switch selectedSegment {
        case .People:
            presenter?.fetchPeopleInfo()
        case .Rooms:
            presenter?.fetchRoomInfo()
        }
    }
}
