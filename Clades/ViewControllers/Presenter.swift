//
//  Presenter.swift
//  Clades
//
//  Created by Oleksiy Chebotarov on 20/09/2019.
//  Copyright © 2019 Oleksiy Chebotarov. All rights reserved.
//

import Foundation
import UIKit

protocol PresenterProtocol {
    func fetchPeopleInfo()
    func fetchRoomInfo()

    var dataFetcherService: DataFetcherProtocol? { get set }
    var viewController: DisplayLogic? { get set }
}

protocol DataStore {
    var persons: [Person]? { get set }
    var rooms: [Room]? { get set }
}

class Presenter: PresenterProtocol, DataStore {
    var persons: [Person]?
    var rooms: [Room]?

    var dataFetcherService: DataFetcherProtocol?
    weak var viewController: DisplayLogic?

    func fetchPeopleInfo() {
        dataFetcherService?.fetchPeopleInfo(completion: { [unowned self] persons in
            self.persons = persons
            self.viewController?.reloadTableView()
        })
    }

    func fetchRoomInfo() {
        dataFetcherService?.fetchRoomInfo(completion: { [unowned self] rooms in
            self.rooms = rooms
            self.viewController?.reloadTableView()
        })
    }
}
