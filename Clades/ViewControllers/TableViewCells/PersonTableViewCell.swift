//
//  PersonTableViewCell.swift
//  Clades
//
//  Created by Oleksiy Chebotarov on 20/09/2019.
//  Copyright © 2019 Oleksiy Chebotarov. All rights reserved.
//

import UIKit

class PersonTableViewCell: UITableViewCell {
    @IBOutlet var photImage: UIImageView!
    @IBOutlet var fullNameLabel: UILabel!
    @IBOutlet var jobTitleLabel: UILabel!
    @IBOutlet var phoneTextView: UITextView!
    @IBOutlet var emailTextView: UITextView!

    func configCell(with person: Person) {
        fullNameLabel.text = person.firstName + " " + person.lastName
        jobTitleLabel.text = person.jobTitle
        phoneTextView.text = person.phone
        emailTextView.text = person.email
        phoneTextView.textContainer.lineFragmentPadding = 0
        phoneTextView.dataDetectorTypes = .all
        emailTextView.textContainer.lineFragmentPadding = 0
        emailTextView.dataDetectorTypes = .all
        photImage.sd_setImage(with: URL(string: person.avatar), placeholderImage: UIImage(named: "placeholder.png"))
        photImage.layer.cornerRadius = 10
        photImage.clipsToBounds = true
    }
}

// MARK: - Accessibility

extension PersonTableViewCell {
    func configureAccessibility(_: Person) {
        photImage.isAccessibilityElement = true
        fullNameLabel.isAccessibilityElement = true
        jobTitleLabel.isAccessibilityElement = true
        phoneTextView.isAccessibilityElement = true
        emailTextView.isAccessibilityElement = true
        phoneTextView.isAccessibilityElement = true

        photImage.accessibilityTraits = .image
        fullNameLabel.accessibilityTraits = .staticText
        fullNameLabel.accessibilityLabel = "Persons name"

        jobTitleLabel.accessibilityTraits = .staticText
        jobTitleLabel.accessibilityLabel = "Job title"

        phoneTextView.accessibilityTraits = .staticText
        phoneTextView.accessibilityLabel = "Phone number"

        emailTextView.accessibilityTraits = .staticText
        emailTextView.accessibilityLabel = "email"

        fullNameLabel.font = UIFont.preferredFont(forTextStyle: .body)
        fullNameLabel.adjustsFontForContentSizeCategory = true

        jobTitleLabel.font = UIFont.preferredFont(forTextStyle: .body)
        jobTitleLabel.adjustsFontForContentSizeCategory = true
    }
}
