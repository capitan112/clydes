//
//  RoomsTableViewCell.swift
//  Clades
//
//  Created by Oleksiy Chebotarov on 20/09/2019.
//  Copyright © 2019 Oleksiy Chebotarov. All rights reserved.
//

import UIKit

class RoomsTableViewCell: UITableViewCell {
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var roomNameLabel: UILabel!
    @IBOutlet var occupiedLabel: UILabel!

    func configCell(with room: Room) {
        nameLabel.text = "Room: "
        roomNameLabel.text = room.name
        if room.isOccupied {
            occupiedLabel.text = "occupied"
            contentView.backgroundColor = UIColor(red: 0.77, green: 0.01, blue: 0.01, alpha: 1.0)
        } else {
            occupiedLabel.text = "free"
            contentView.backgroundColor = UIColor.green
        }

        contentView.layer.cornerRadius = 8
        contentView.layer.masksToBounds = true
    }
}

// MARK: - Accessibility

extension RoomsTableViewCell {
    func configureAccessibility(_: Room) {
        nameLabel.isAccessibilityElement = true
        roomNameLabel.isAccessibilityElement = true
        occupiedLabel.isAccessibilityElement = true

        nameLabel.accessibilityTraits = .staticText
        nameLabel.accessibilityLabel = "Name label"

        roomNameLabel.accessibilityTraits = .staticText
        roomNameLabel.accessibilityLabel = "Room"

        occupiedLabel.accessibilityTraits = .staticText
        occupiedLabel.accessibilityLabel = "is occupied"

        nameLabel.font = UIFont.preferredFont(forTextStyle: .body)
        nameLabel.adjustsFontForContentSizeCategory = true

        roomNameLabel.font = UIFont.preferredFont(forTextStyle: .body)
        roomNameLabel.adjustsFontForContentSizeCategory = true

        occupiedLabel.font = UIFont.preferredFont(forTextStyle: .body)
        occupiedLabel.adjustsFontForContentSizeCategory = true
    }
}
