//
//  DataFetcherService.swift
//  Clades
//
//  Created by Oleksiy Chebotarov on 20/09/2019.
//  Copyright © 2019 Oleksiy Chebotarov. All rights reserved.
//

import Foundation
import UIKit

protocol DataFetcherProtocol {
    func fetchPeopleInfo(completion: @escaping ([Person]?) -> Void)
    func fetchRoomInfo(completion: @escaping ([Room]?) -> Void)
}

class DataFetcherService: DataFetcherProtocol {
    let peopleUrlString = "https://5cc736f4ae1431001472e333.mockapi.io/api/v1people"
    let roomsUrlString = "https://5cc736f4ae1431001472e333.mockapi.io/api/v1rooms"

    var networkDataFetcher: NetworkDataFetcherProtocol

    init(networkDataFetcher: NetworkDataFetcher = NetworkDataFetcher()) {
        self.networkDataFetcher = networkDataFetcher
    }

    func fetchPeopleInfo(completion: @escaping ([Person]?) -> Void) {
        networkDataFetcher.fetchGenericJSONData(urlString: peopleUrlString, response: completion)
    }

    func fetchRoomInfo(completion: @escaping ([Room]?) -> Void) {
        networkDataFetcher.fetchGenericJSONData(urlString: roomsUrlString, response: completion)
    }
}
