//
//  GCDBlackBox.swift
//  Clades
//
//  Created by Oleksiy Chebotarov on 20/09/2019.
//  Copyright © 2019 Oleksiy Chebotarov. All rights reserved.
//

import Foundation

func performUIUpdatesOnMain(_ updates: @escaping () -> Void) {
    DispatchQueue.main.async {
        updates()
    }
}
