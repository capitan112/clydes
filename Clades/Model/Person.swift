//
//  People.swift
//  Clades
//
//  Created by Oleksiy Chebotarov on 20/09/2019.
//  Copyright © 2019 Oleksiy Chebotarov. All rights reserved.
//

import Foundation

struct Person: Codable {
    var id: String
    var createdAt: String
    var avatar: String
    var jobTitle: String
    var phone: String
    var favouriteColor: String
    var email: String
    var firstName: String
    var lastName: String
}
